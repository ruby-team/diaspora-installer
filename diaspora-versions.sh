#!/bin/sh
set -e
# Show current version of package
# This needs to be embedded inside main scripts, $2 is installed version
export installed_diaspora_version=`echo $2|cut -d+ -f1`
export installed_diaspora_major_version=`echo ${installed_diaspora_version} |cut -d. -f1,2`
export installed_diaspora_minor_version=`echo ${installed_diaspora_version} |cut -d. -f3,4`
export diaspora_minor_version=`echo ${diaspora_version} |cut -d. -f3,4`
