Template: diaspora-common/url
Type: string
Default: localhost
_Description: Host name for this instance of Diaspora:
 Please choose the host name which should be used to access this
 instance of Diaspora.
 .
 This should be the fully qualified name as seen from the Internet, with
 the domain name that will be used to access the pod.
 .
 If a reverse proxy is used, give the hostname that the proxy server
 responds to.
 .
 This host name should not be modified after the initial setup because
 it is hard-coded in the database.

Template: diaspora-common/dbpass
Type: note
_Description: PostgreSQL application password
 You can leave the PostgreSQL application password blank, as the "ident"
 authentication method is used, allowing the diaspora user on the system
 to connect to the Diaspora database without a password.

Template: diaspora-common/ssl
Type: boolean
Default: true
_Description: Enable https?
 Enabling https means that an SSL/TLS certificate is required to access this
 Diaspora instance (as Nginx will be configured to respond only to https
 requests). A self-signed certificate is enough for local testing (and
 can be generated using, for instance, the package easy-rsa), but will
 not be accepted for federation with other Diaspora pods.
 .
 Some certificate authorities like Let's Encrypt (letsencrypt.org), StartSSL
 (startssl.com) offer free SSL/TLS certificates that work with Diaspora;
 however, certificates provided by CAcert will not work with Diaspora.
 .
 Nginx must be reloaded after the certificate and key files are made available
 at /etc/diaspora/ssl. letsencrypt package may be used to automate interaction
 with Let's Encrypt to obtain a certificate.
 .
 You can disable https if you want to access Diaspora only locally or you don't
 want to federate with other diaspora pods.

Template: diaspora-common/letsencrypt
Type: boolean
Default: false
_Description: Use Let's Encrypt?
 Symbolic links to certificate and key created using letsencrypt package
 (/etc/letencrypt/live) will be added to /etc/diaspora/ssl if this option is
 selected.
 .
 Otherwise, certificate and key files have to be placed manually to
 /etc/diaspora/ssl directory as '<host name>-bundle.crt' and '<host name>.key'.
 .
 Nginx will be stopped, if this option is selected, to allow letsencrypt to use
 ports 80 and 443 during domain ownership validation and certificate retrieval
 step.
 .
 Note: letsencrypt does not have a usable nginx plugin currently, so
 certificates must be renewed manually after 3 months, when current
 letsencrypt certificate expire. If you choose this option, you will also be
 agreeing to letsencrypt terms of service.

Template: diaspora-common/letsencrypt_email
Type: string
_Description: Email address for letsencrypt updates:
 Please provide a valid email address for letsencrypt updates.

Template: diaspora-common/dbbackup
Type: note
_Description: Backup your database
 This upgrade includes long running migrations that can take hours to complete
 on large pods. It is adviced to take a backup of your database.
 .
 Commands to backup and restore database is given below (run as root user):
 .
 # su postgres -c 'pg_dump diaspora_production -f /var/lib/postgresql/diaspora_production.sql'
 .
 # su postgres -c 'psql -d diaspora_production -f /var/lib/postgresql/diaspora_production.sql'

Template: diaspora-common/services
Type: multiselect
Choices: Facebook, Twitter, Tumblr, Wordpress
_Description: Third party services to be enabled: 
 Diaspora can connect with different services.
 .
 When a diaspora instance is connected to a third party service,  it allows
 any user of this diaspora instance to link their account on that service to
 their diaspora account and send their updates to that service if they choose
 the service when publishing a post.

Template:diaspora-common/facebook_app_id
Type: string
_Description: Facebook App ID:
 Give your Facebook App ID. This can not be blank.

Template:diaspora-common/facebook_secret
Type: password
_Description: Facebook Secret:
 Give your Facebook Secret. This can not be blank.

Template:diaspora-common/twitter_key
Type: string
_Description: Twitter Key:
 Give your Twitter Key. This can not be blank.

Template:diaspora-common/twitter_secret
Type: password
_Description: Twitter Secret:
 Give your Twitter Secret. This can not be blank.

Template:diaspora-common/tumblr_key
Type: string
_Description: Tumblr Key:
 Give your Tumblr Key. This can not be blank.

Template:diaspora-common/tumblr_secret
Type: password
_Description: Tumblr Secret:
 Give your Tumblr Secret. This can not be blank.

Template:diaspora-common/wordpress_client_id
Type: string
_Description: Wordpress Client ID:
 Give your Wordpress Client ID. This can not be blank.

Template:diaspora-common/wordpress_secret
Type: password
_Description: Wordpress Secret:
 Give your Wordpress Secret. This can not be blank.

Template: diaspora-common/purge_data
Type: boolean
Default: true
_Description: Remove all data?
 This will permanently remove all data of this Diaspora instance such as
 uploaded files and any customizations in homepage.
