#! /bin/sh
# Read configuration values	
set -e
. /etc/diaspora/diaspora-common.conf

# Create an empty public/source.tar.gz for diaspora package
# script/server checks for its existence
su diaspora -s /bin/sh -c 'test -f public/source.tar.gz || touch public/source.tar.gz'

reload_nginx() {
    if command -v nginx > /dev/null
    then
	echo "Reloading nginx..."
	invoke-rc.d nginx reload
    else
	echo "Nginx unavailable!"
	exit 1
    fi
}

if grep https ${diaspora_conf}
then
    if test -f ${diaspora_ssl_path}/$SERVERNAME-bundle.pem
    then
	reload_nginx
    else
	mkdir -p ${diaspora_ssl_path}
	echo "Copy $SERVERNAME-bundle.pem and $SERVERNAME.key to /etc/diaspora/ssl"
	echo "And reload nginx, run # /etc/init.d/nginx reload"
    fi
else
    reload_nginx
fi

echo "Debian specific documentation is available at /usr/share/doc/diaspora-common/README"

echo "Visit your pod at $ENVIRONMENT_URL"
